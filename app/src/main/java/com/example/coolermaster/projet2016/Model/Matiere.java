package com.example.coolermaster.projet2016.Model;

/**
 * Created by root on 12/12/16.
 */

public class Matiere {
    private int id;
    private String nom_cours;

    public Matiere(int id, String nom_cours) {
        this.id = id;
        this.nom_cours = nom_cours;
    }

    public int getId() {
        return id;
    }

    public String getNom_cours() {
        return nom_cours;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Matiere(int id) {
        this.id = id;
    }

    public void setNom_cours(String nom_cours) {

        this.nom_cours = nom_cours;
    }
}
