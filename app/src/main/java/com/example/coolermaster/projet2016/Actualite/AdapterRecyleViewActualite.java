package com.example.coolermaster.projet2016.Actualite;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.coolermaster.projet2016.Model.Actualite;
import com.example.coolermaster.projet2016.R;

public class AdapterRecyleViewActualite extends RecyclerView.Adapter<AdapterRecyleViewActualite.MyViewHolder>
{






    @Override
    public int getItemCount() {
        return JsonActualiteShow.actualiteList.size();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_actualite, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Actualite pair = JsonActualiteShow.actualiteList.get(position);
        holder.display(pair);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView description;
        private final TextView date;
        private final TextView auteur;

        private Actualite currentPair = new Actualite();
        public MyViewHolder(final View itemView) {
            super(itemView);
            name = ((TextView) itemView.findViewById(R.id.nom_actualite));
            description = ((TextView)
                    itemView.findViewById(R.id.contenu_actualite));
            date = ((TextView) itemView.findViewById(R.id.date_actualite));
            auteur = ((TextView)
                    itemView.findViewById(R.id.auteur_actualite));




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(currentPair.getTitre())
                            .setMessage(currentPair.getContenu())
                            .show();
                }
            });
        }
        public void display(Actualite ev) {
            currentPair = ev;
            name.setText(ev.getTitre());
            description.setText(ev.getContenu());
            date.setText(ev.getDate());
            Log.d("DATEEEEEEEEEEEEEEEEEEE",ev.getDate());
            auteur.setText(ev.getEnseignant().toString());
        }
    }

}