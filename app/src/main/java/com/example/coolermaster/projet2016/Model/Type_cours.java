package com.example.coolermaster.projet2016.Model;

/**
 * Created by root on 12/12/16.
 */

public class Type_cours {
    private int id;
    private String nom;

    public Type_cours(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public Type_cours(int id) {

        this.id = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
