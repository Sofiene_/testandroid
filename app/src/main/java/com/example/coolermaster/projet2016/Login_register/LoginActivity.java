package com.example.coolermaster.projet2016.Login_register;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.coolermaster.projet2016.Actualite.JsonActualiteShow;
import com.example.coolermaster.projet2016.Chat.Rooms;
import com.example.coolermaster.projet2016.Draw.draw;
import com.example.coolermaster.projet2016.MainActivity;
import com.example.coolermaster.projet2016.Evenement.ParserActivity;
import com.example.coolermaster.projet2016.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;
import static com.example.coolermaster.projet2016.Actualite.JsonActualiteShow.usedJson;
import static com.example.coolermaster.projet2016.MainActivity.group;
import static com.example.coolermaster.projet2016.MainActivity.nameUSER;
import static com.example.coolermaster.projet2016.MainActivity.userType;
import static com.example.coolermaster.projet2016.Evenement.ParserActivity.usedParser;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    public static String id_enseignant;
    String username = "";
    String password = "";
public static Boolean ready = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        group="1";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        mPasswordView = (EditText) findViewById(R.id.password);
        if (!usedParser) {
            new ParserActivity().execute();
        //new Rooms().onCreate(new Bundle());
            }


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

 //Coder a enlever lors de la soutenance /////////////////////////////////////////////////////////////////////////////////////////////////////
        Button Passer = (Button) findViewById(R.id.passer_direct);
        Passer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });



       /* Button test_b = (Button) findViewById(R.id.test_id);
        test_b.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,draw.class);
                startActivity(intent);
            }
        });
*/
//FIN CODE////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        TextView register_lien = (TextView) findViewById(R.id.register_lien_id);
        register_lien.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegistarActivity.class);
                startActivity(intent);
            }
        });







        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


    }


  /*  public void invokeLogin(){

               new Login().execute();

    }
class Login extends AsyncTask<String,Void,String> {
    @Override
    protected String doInBackground(String... params) {

        try {



        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error", e.getMessage() + " test");
        }
        // login(username,password
     return nameUSER;}

            @Override
            protected void onPostExecute(String y){
                ready = true;
                if(!y.equalsIgnoreCase("ENICARTHAGE")){
            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            finish();
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(), "Invalid User Name or Password", Toast.LENGTH_LONG).show();
        }
    }
    @Override protected void onPreExecute () {
        nameUSER = "ENICARTHAGE";
        username = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();
        attemptLogin();
Log.d("EMAIL","a " +username+" a");
        Log.d("PASSSSSSSSS","a "+password+" a");
    }

    }*/





    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or RegistarActivity the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            String elements1;
            String elements2;
            try {
                Document document1 = Jsoup.connect("http://androidproject2016.hostei.com/selectenseignant.php").get();
                Document document2 = Jsoup.connect("http://androidproject2016.hostei.com/selectetudiant.php").get();
                 elements1 = document1.text();
                 elements2 = document2.text();
                Log.d("----elements1-----",elements1);
                Log.d("----elements2-----",elements2);
                // Simulate network access.
                //Thread.sleep(1000);
          //  } catch (InterruptedException e) {
            //    return false;
            }catch (SocketTimeoutException e) {
                Toast.makeText(getApplicationContext(), "Connexion Error !!!",Toast.LENGTH_LONG).show();
                e.printStackTrace();
                Log.e("Error", "a"+e.getMessage());
                return false;
            }catch (UnknownHostException e) {
                Toast.makeText(getApplicationContext(), "Connexion Error !!!",Toast.LENGTH_LONG).show();
                e.printStackTrace();
                Log.e("Error", "a"+e.getMessage());
                return false;}
            catch (Exception e) {
                e.printStackTrace();
                Log.e("Error", "a"+e.getMessage());
                return false;
            }


            for (String credential : elements1.split("!")) {
                Log.d("SPLIT ELEMENTS 1",credential);
                 if (credential != null) {

                String[] pieces = credential.split(":");
                if (pieces[1].equals(mEmail)) {
                    id_enseignant=pieces[0];
                    Log.d("ID ENSEIGNANT",id_enseignant);
                    nameUSER=pieces[3]+" "+pieces[4];
                    userType="Enseignant";
                    // Account exists, return true if the password matches.
                    return pieces[2].equals(mPassword);
                }
            }
            }

            for (String credential : elements2.split("!")) {
                Log.d("SPLIT ELEMENTS 2----",credential);
                if (credential != null ) {
                    String[] pieces = credential.split(":");
                    if (pieces[1].equals(mEmail)) {
                        nameUSER=pieces[3]+" "+pieces[4];
                        group=pieces[6];
                        Log.d("GROOOOOOOOOOOUP","number = "+group);
                        userType="Etudiant";
                        // Account exists, return true if the password matches.
                        return pieces[2].equals(mPassword);
                    }
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            Log.d("nameuser","= "+nameUSER);
            Log.d("usertype","= "+userType);
            if (success) {
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                Toast.makeText(getApplicationContext(), "Welcome "+nameUSER+" !",Toast.LENGTH_LONG).show();
                 startActivity(intent);
                finish();
            } else {
               /* mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();*/
                Toast.makeText(getApplicationContext(), "Invalid User Name or Password", Toast.LENGTH_LONG).show();
            }
        }
        @Override
        protected  void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

