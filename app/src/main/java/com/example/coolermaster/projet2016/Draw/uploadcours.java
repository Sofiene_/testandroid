package com.example.coolermaster.projet2016.Draw;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by root on 12/12/16.
 */

public class uploadcours extends AsyncTask<Void ,Void,Void> {


    private Activity activity;
    private Bitmap cours;
    private String nom_cours;
    private int id_matiere;
    private int id_type_cours;
    private int id_enseignant;
    private int id_groupe;
    ProgressDialog loading;


    public uploadcours(Activity activity, Bitmap cours, String nom_cours, int id_matiere, int id_type_cours, int id_enseignant, int id_groupe) {
        this.activity = activity;
        this.cours = cours;
        this.nom_cours = nom_cours;
        this.id_matiere = id_matiere;
        this.id_type_cours = id_type_cours;
        this.id_enseignant = id_enseignant;
        this.id_groupe = id_groupe;

    }

    public static final String UPLOAD_URL = "http://androidproject2016.hostei.com/insertcours.php";



    @Override
    protected Void doInBackground(Void... params) {

        //CONVERTIR EN CHANE DE CARACTERE
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        cours.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        String encode_cours = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        //////////////

        //SEND DATA
        ArrayList<NameValuePair> dataToSend = new ArrayList<>();


        dataToSend.add(new BasicNameValuePair("cours",encode_cours));
        dataToSend.add(new BasicNameValuePair("nom_cours",nom_cours+""));
        dataToSend.add(new BasicNameValuePair("id_matiere",id_matiere+""));
        dataToSend.add(new BasicNameValuePair("type_cours",id_type_cours+""));
        dataToSend.add(new BasicNameValuePair("id_enseignant",id_enseignant+""));
        dataToSend.add(new BasicNameValuePair("id_groupe",id_groupe+""));

        HttpParams httpRequestParams =gethttpRequestParams();

        HttpClient client = new DefaultHttpClient(httpRequestParams);
        HttpPost post = new HttpPost(UPLOAD_URL);
        try {

            post.setEntity(new UrlEncodedFormEntity(dataToSend));
            client.execute(post);


        }catch (Exception e )
        {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        loading.dismiss();
        Toast.makeText(activity, "Image Uploaded", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        loading = ProgressDialog.show(activity, "Uploading Image", "Please wait...",true,true);
    }



    private HttpParams gethttpRequestParams()
    {
        HttpParams httpRequestParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpRequestParams,1000*30);
        HttpConnectionParams.setSoTimeout(httpRequestParams,1000 * 30);
        return httpRequestParams;
    }
}
