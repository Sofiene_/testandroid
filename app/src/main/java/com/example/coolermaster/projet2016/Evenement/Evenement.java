package com.example.coolermaster.projet2016.Evenement;

/**
 * Created by COOLER MASTER on 06/12/2016.
 */

public class Evenement {
    private String Date;
    private String Text;
    private String Link;

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }


    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }
}
