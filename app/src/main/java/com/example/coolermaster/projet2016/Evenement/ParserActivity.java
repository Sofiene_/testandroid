package com.example.coolermaster.projet2016.Evenement;

import android.os.AsyncTask;
import android.util.Log;

import com.example.coolermaster.projet2016.Evenement.Evenement;
import com.example.coolermaster.projet2016.MainActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import static android.app.PendingIntent.getActivity;

/**
 * Created by COOLER MASTER on 06/12/2016.
 */
public class ParserActivity extends AsyncTask<ArrayList<Evenement>,Void,ArrayList<Evenement>> {
   // public static ProgressDialog dialog;
           public static ArrayList<Evenement> Evenements = new ArrayList<>();
public static Boolean error = false;
    public static Boolean usedParser = false;
    @Override
    protected ArrayList<Evenement> doInBackground(ArrayList<Evenement>... Params) {
        usedParser = true;
        Document document;
        ArrayList<Evenement> c1 = new ArrayList<>();
        String URL = "http://www.enicarthage.rnu.tn/";

        try {

            // need http protocol
      document = Jsoup.connect(URL).get();


          Evenement item;

            //Element classement = document.getElementById("classement");

           Elements elements = document.getElementsByClass("latestnews");
         //   Log.d("DDDDDDDDDDDDDDDDDDDD",""+elements.toString());
            boolean ignorefirstiteration = true;
            for (Element e : elements) {
           item = new Evenement();


                String link = e.getElementsByClass("latestnews").first().getElementsByTag("a").first().attr("href").toString();
                link= link.substring(1);
/*
                String subURL = URL + link;
                Document  subDocument= Jsoup.connect(subURL).get();
                Log.d("___ START CONTENT _____",subDocument.toString());
                        Log.d("________END CONTENT","______");*/
                String titre = e.getElementsByClass("latestnews").first().getElementsByTag("span").text();
                String Descrip = e.getElementsByClass("latestnews").first().getElementsByTag("a").first().attr("title").toString();
          if (ignorefirstiteration || titre.toUpperCase() == titre.toLowerCase()) { ignorefirstiteration = false ;continue;}
           //     Log.d("EEEEEEEEEEEEEEEEEEEE",e.toString());
                int i=    Descrip.indexOf("||");
                titre = Descrip.substring(0,i)+'\n' + titre;
                Descrip = Descrip.substring(i+2);
          item.setDate(titre);
          item.setText(Descrip);
                item.setLink(URL+link);
               // Log.d("HHHHHHHHHHHHHHHHHHHHH",link);
             //   Log.d("aaaaaaaaaa",titre);
           //     Log.d("bbbbbbbbbbb",Descrip);
               c1.add(item);

        }

     }  catch (UnknownHostException e) {
            usedParser=false;
            error = true;
            e.printStackTrace();
            Log.e("Error de conx", e.getMessage());}
        catch (SocketTimeoutException e)  {
            usedParser=false;
            error = true;
            e.printStackTrace();
            Log.e("Error de conx", "writing here to not get an error "+e.getMessage());}
        catch (Exception e) {
            usedParser=false;
         e.printStackTrace();
           Log.e("Error", e.getMessage());
        }
        Thread downloadThread = new Thread() {
            public void run() {
                Document doc;
                try {
                    doc = Jsoup.connect("http://www.google.com/").get();
                    String title = doc.title();
                    System.out.print(title);
                }
                catch (UnknownHostException e) {
                    error = true;
                    e.printStackTrace();
                    Log.e("Error de conx", "a"+e.getMessage());}
                catch (SocketTimeoutException e)  {
                    error = true;
                    e.printStackTrace();
                    Log.e("Error de conx", "a"+e.getMessage());}
                 catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        downloadThread.start();

        return c1;
    }

    @Override
protected  void onPreExecute ( ) {

    super.onPreExecute();



}

    @Override
    protected void onPostExecute( ArrayList<Evenement> ts) {
      //  super.onPostExecute();
      try {
            if (Evenements == null)
                Evenements = new ArrayList<>();
//dialog.dismiss();
            Evenements.addAll(ts);
          MainActivity.adapterRecyleViewEvent.notifyDataSetChanged();
        } catch (Exception e) {

        }

    }
}