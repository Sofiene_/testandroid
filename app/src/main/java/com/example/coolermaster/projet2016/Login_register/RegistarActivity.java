package com.example.coolermaster.projet2016.Login_register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import org.jsoup.nodes.Document;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.coolermaster.projet2016.R;

import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RegistarActivity extends AppCompatActivity implements OnItemSelectedListener  {



    private EditText editTextnom,editTextprenom;
    private EditText editTextpassword;
    private EditText editTextgroupe,editTextnum_inscription;
    private AutoCompleteTextView editTextmail;
    private Button buttonRegister;
    private Button buttonCancel;
    public static String groupselected;
    public static  List<String> mails = new ArrayList<>();
    String groupe;

    private static final String REGISTER_URL = "http://androidproject2016.hostei.com/insertetudiant.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.groupe_id);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("3 Si A");
        categories.add("3 Si B");
        categories.add("3 Si C");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);



        editTextnom = (EditText) findViewById(R.id.nom_id);
        editTextprenom = (EditText) findViewById(R.id.prenom_id);
        editTextmail = (AutoCompleteTextView) findViewById(R.id.mail_id);
        editTextpassword = (EditText) findViewById(R.id.password_id);
       // Spinner spinner = (Spinner) findViewById(R.id.groupe_id);
        groupselected = spinner.getSelectedItem().toString();
        editTextnum_inscription = (EditText) findViewById(R.id.num_inscription_id);
        buttonRegister = (Button) findViewById(R.id.register_id);
        buttonCancel =(Button) findViewById(R.id.cancel_id);



        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                  }
        });


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
        new MailVerifTask().execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        groupe=item.trim().toLowerCase();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
    private boolean doesEmailExist(String email) {
        return mails.contains(email);

    }

    private void registerUser() {
        String nom = editTextnom.getText().toString().trim().toLowerCase();
        String prenom = editTextprenom.getText().toString().trim().toLowerCase();
        String mail = editTextmail.getText().toString().trim().toLowerCase();
        String password = editTextpassword.getText().toString().trim().toLowerCase();
        int group;
        Log.d("LE GROUPE EST  =",groupe) ;
        switch (groupe)
        {
            case "3 si a":
    group=2;break;
            case "3 si b":
                group=3;
                break;
            case "3 si c":
                group=4;
            break;
            default:
    group=1;
        }

        String num_inscription = editTextnum_inscription.getText().toString().trim().toLowerCase();

        editTextmail.setError(null);
        editTextpassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(nom)) {
            editTextnom.setError(getString(R.string.error_field_required));
            focusView = editTextnom;
            cancel = true;
        }
        else if (TextUtils.isEmpty(prenom)) {
            editTextprenom.setError(getString(R.string.error_field_required));
            focusView = editTextprenom;
            cancel = true;
        }
        else if (TextUtils.isEmpty(num_inscription)) {
            editTextnum_inscription.setError(getString(R.string.error_field_required));
            focusView = editTextnum_inscription;
            cancel = true;
        }
        else if (TextUtils.isEmpty(password)) {
            editTextpassword.setError(getString(R.string.error_field_required));
            focusView = editTextpassword;
            cancel = true;
        }
        else if (!isPasswordValid(password)) {
            editTextpassword.setError(getString(R.string.error_invalid_password));
            focusView = editTextpassword;
            cancel = true;
        }
        else if (doesEmailExist(mail)) {
            editTextmail.setError(getString(R.string.error_email_exist));
            focusView = editTextmail;
            cancel = true;
        }
        // Check for a valid email address.
        else if (TextUtils.isEmpty(mail)) {
            editTextmail.setError(getString(R.string.error_field_required));
            focusView = editTextmail;
            cancel = true;
        } else if (!isEmailValid(mail)) {
            editTextmail.setError(getString(R.string.error_invalid_email));
            focusView = editTextmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            register(nom,prenom,mail,password,group,num_inscription);
        }




    }

    private void register(String nom, String prenom, String mail, String password , int groupe , String num_inscription) {
        String urlSuffix = "?nom="+nom+"&prenom="+prenom+"&mail="+mail+"&password="+password+"&id_groupe="+groupe+"&num_inscription="+num_inscription;
        Log.d("URL SUFFIX",urlSuffix);
        class RegisterUser extends AsyncTask<String, Void, String>{

                            ProgressDialog loading;


                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        loading = ProgressDialog.show(RegistarActivity.this, "Please Wait",null, true, true);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        //super.onPostExecute(s);
                        loading.dismiss();
                        Log.d("STRING S TOAST",s+"end");
                        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
                        if (s.contains("successfully")) {
                            Intent intent = new Intent(RegistarActivity.this,LoginActivity.class);
                            startActivity(intent); }
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String s = params[0];
                        Log.d("STRING S",s);
                        BufferedReader bufferedReader = null;
                        try {
                            URL url = new URL(REGISTER_URL+s);
                    Document document = Jsoup.connect(url.toString()).get();
                    Log.d("STRING URL",url.toString());
                    String result;

                    result = document.text();
                    Log.d("STRING result",result);
                    return result;
                }catch(Exception e){
                   e.printStackTrace();
                    Log.e("Error thread register", e.getMessage());
                return null;
                }
            }
        }

        RegisterUser ru = new RegisterUser();
        Log.d("url suffix execute",urlSuffix);
        ru.execute(urlSuffix);
    }




    public class MailVerifTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            String elements1 = "";
            String elements2 = "";
            try {
                Document document1 = Jsoup.connect("http://androidproject2016.hostei.com/selectenseignant.php").get();
                Document document2 = Jsoup.connect("http://androidproject2016.hostei.com/selectetudiant.php").get();
                elements1 = document1.text();
                elements2 = document2.text();
                // Simulate network access.
                //Thread.sleep(1000);
                //  } catch (InterruptedException e) {
                //    return false;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Error", e.getMessage());
            }
            for (String credential : elements1.split("!")) {
                if (credential != null) {
                    String[] pieces = credential.split(":");
                    mails.add(pieces[1]);
                }
            }

            for (String credential : elements2.split("!")) {
                if (credential != null) {
                    String[] pieces = credential.split(":");
                    mails.add(pieces[1]);

                }
            }

       return null; }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

    }

}