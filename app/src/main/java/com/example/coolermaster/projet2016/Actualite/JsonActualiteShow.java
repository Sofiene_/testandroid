package com.example.coolermaster.projet2016.Actualite;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.coolermaster.projet2016.Model.Actualite;
import com.example.coolermaster.projet2016.MainActivity;
import com.example.coolermaster.projet2016.Model.Enseignant;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.example.coolermaster.projet2016.MainActivity.group;

/**
 * Created by COOLER MASTER on 09/12/2016.
 */

      public class JsonActualiteShow extends AsyncTask<ArrayList<Actualite>,Void,ArrayList<Actualite>> {



            private static String getActualite_URL = "androidproject2016.hostei.com/selectactualites2.php";
            String myJSON ="";
            private static final String TAG_RESULTS="result";
            // private static final String TAG_ID = "id";
            private static final String TAG_NAME = "nom_actualite";
            private static final String TAG_ADD ="contenu_actualite";
            public static Boolean usedJson=false;

            JSONArray actualites = null;

            public static ArrayList<Actualite> actualiteList = new ArrayList<>();



            @Override
            protected ArrayList<Actualite> doInBackground(ArrayList<Actualite>... Params) {
                usedJson=true;
                ArrayList<Actualite> c1 = new ArrayList<>();
         //       Log.d("test","test");
                Uri uri;
        if (group.equals("1")) { uri = new Uri.Builder()
        .scheme("http")
        .authority("androidproject2016.hostei.com")
        .path("/selectactualites.php")
        .build();}
                else { uri = new Uri.Builder()
        .scheme("http")
        .authority("androidproject2016.hostei.com")
        .path("/selectactualites2.php")
        .appendQueryParameter("id_groupe", group)
        .build();}
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());

                Log.d("URI BUILDER TEST",uri.toString());
                HttpGet httpGet = new HttpGet(uri.toString());
               // httpGet.setHeader("id_groupe",group); FONCTIONNE PAS

                Log.d("REQUEST HTTP",httpGet.toString());
                InputStream inputStream = null;
                try {
                    HttpResponse response = httpclient.execute(httpGet);
                    Log.d("response",response.toString());
                    HttpEntity entity = response.getEntity();
                    Log.d("entity",entity.toString());

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null)
                    {
                 //ss       Log.d("LINE READER", line);
                        sb.append((line + '\n'));
                    }

                    myJSON= sb.toString();
                    Log.d("----myJson affichage---",myJSON);

                    JSONObject jsonObj = new JSONObject(myJSON);

                    actualites = jsonObj.getJSONArray(TAG_RESULTS);
                  //  Log.d("5555555555555555555","555555555555555555");
                    for(int i=0;i<actualites.length();i++){
                        JSONObject c = actualites.getJSONObject(i);
                        Actualite actualite = new Actualite();
                        Log.d("ACTUALITE : ","a"+c.toString());
                        actualite.setTitre(c.getString("titre"));
                        Log.d("ACTUALITE TITRE","a"+actualite.getTitre());
                        actualite.setContenu(c.getString("contenu"));
                        Log.d("ACTUALITE CONTENU","a"+actualite.getTitre());
                        actualite.setDate(c.getString(("date_publication")));
                        Log.d("ACTUALITE DATE","a"+actualite.getDate());
                        actualite.setEnseignant(new Enseignant(1,c.getString("nom"),c.getString("prenom")));
                        Log.d("NOM PRENOM ENSEIGNANT","a"+actualite.getEnseignant().toString());
                        c1.add(actualite);
                    }

                }


                catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Error at doinbackground", e.getMessage());
                }
                finally {
                    try{if(inputStream != null)inputStream.close();}catch(Exception squish){squish.printStackTrace();Log.e("Error", squish.getMessage());}
                }
                return c1;
            }

            @Override
            protected void onPostExecute(ArrayList<Actualite> ts){
                try {
                    if (actualiteList == null)
                        actualiteList = new ArrayList<>();
                    actualiteList.clear();
                    actualiteList.addAll(ts);
                    MainActivity.adapterRecyleViewActualite.notifyDataSetChanged();
                    usedJson=true;
                }

                catch (Exception e) {
                e.printStackTrace();
                    Log.e("Error at  Onpostexecute", e.getMessage());
                }

            }


        }

