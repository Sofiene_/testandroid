package com.example.coolermaster.projet2016.Model;

import android.graphics.Bitmap;

/**
 * Created by root on 12/12/16.
 */

public class cours {
    private int id;
    private String nom_cours;
    private Bitmap fich_cours;
    private Matiere matiere;
    private Type_cours tc;
    private  Enseignant enseignant;
    private Groupe groupe;

    public cours(int id, String nom_cours, Bitmap fich_cours, Type_cours tc, Matiere matiere, Enseignant enseignant, Groupe groupe) {
        this.id = id;
        this.nom_cours = nom_cours;
        this.fich_cours = fich_cours;
        this.tc = tc;
        this.matiere = matiere;
        this.enseignant = enseignant;
        this.groupe = groupe;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom_cours(String nom_cours) {
        this.nom_cours = nom_cours;
    }

    public void setFich_cours(Bitmap fich_cours) {
        this.fich_cours = fich_cours;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public void setTc(Type_cours tc) {
        this.tc = tc;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public int getId() {

        return id;
    }

    public String getNom_cours() {
        return nom_cours;
    }

    public Bitmap getFich_cours() {
        return fich_cours;
    }

    public Matiere getMatiere() {
        return matiere;
    }

    public Type_cours getTc() {
        return tc;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public cours(String nom_cours, Bitmap fich_cours, Matiere matiere, Type_cours tc, Enseignant enseignant, Groupe groupe) {

        this.nom_cours = nom_cours;
        this.fich_cours = fich_cours;
        this.matiere = matiere;
        this.tc = tc;
        this.enseignant = enseignant;
        this.groupe = groupe;
    }
}
