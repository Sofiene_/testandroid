package com.example.coolermaster.projet2016.Actualite;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.coolermaster.projet2016.Login_register.LoginActivity;
import com.example.coolermaster.projet2016.Login_register.RegistarActivity;
import com.example.coolermaster.projet2016.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.coolermaster.projet2016.Login_register.LoginActivity.id_enseignant;
import static com.example.coolermaster.projet2016.MainActivity.group;

/**
 * Created by COOLER MASTER on 12/12/2016.
 */

public class InsertActualite extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ProgressDialog loadingActu;
    private EditText editTexttitle, editTextcontent;
    private Spinner spinner;

    private Button buttonSave;
    private Button buttonCancel;
    public static String groupselected;
    int id_groupe;

    private static final String InsertActualite_URL = "http://androidproject2016.hostei.com/insertActualite.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insert_actualite);        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.actualitegroup);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("3 Si A");
        categories.add("3 Si B");
        categories.add("3 Si C");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);


        editTexttitle = (EditText) findViewById(R.id.actualitetitle);
        editTextcontent = (EditText) findViewById(R.id.actualitecontent);
        // Spinner spinner = (Spinner) findViewById(R.id.groupe_id);
        groupselected = spinner.getSelectedItem().toString();
        buttonSave = (Button) findViewById(R.id.actualiteSave);
        buttonCancel = (Button) findViewById(R.id.actualitecancel);


        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActualite(editTexttitle.getText().toString(),editTextcontent.getText().toString(),groupselected,id_enseignant);
            }
        });}

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        groupselected = item.trim().toLowerCase();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }







    private void addActualite(String titre, String contenu, String groupe , String id_enseignant) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
         //2016/11/16 12:08:43
        switch (groupe)
        {
            case "3 si a":
                id_groupe=2;break;
            case "3 si b":
                id_groupe=3;
                break;
            case "3 si c":
                id_groupe=4;
                break;
            default:
                id_groupe=1;
        }
        // START CHANGING TITRE + CONTENU TO URL STYLE OR SOMETHING haha :P
        String[] words1 = titre.split(" ");
        StringBuilder sentence1 = new StringBuilder(words1[0]);

        for (int i = 1; i < words1.length; ++i) {
            sentence1.append("%20");
            sentence1.append(words1[i]);
        }
        titre.toString();
        Log.d("TITRE ",titre);

        String[] words2 = contenu.split(" ");
        StringBuilder sentence2 = new StringBuilder(words2[0]);

        for (int i = 1; i < words2.length; ++i) {
            sentence2.append("%20");
            sentence2.append(words2[i]);
        }
        contenu = sentence2.toString();
        Log.d("CONTENU",contenu);
        //END CHANGING
        String urlSuffix = "?titre="+titre+"&contenu="+contenu+"&date_publication="+dateFormat.format(date)+"&id_enseignant="+id_enseignant+"&id_groupe="+id_groupe;
        Log.d("URL SUFFIX Actu",urlSuffix);
        class addActualiteclass extends AsyncTask<String, Void, String> {

            ProgressDialog loading;



            public String replace(String str) {
                String[] words = str.split(" ");
                StringBuilder sentence = new StringBuilder(words[0]);

                for (int i = 1; i < words.length; ++i) {
                    sentence.append("%20");
                    sentence.append(words[i]);
                }

                return sentence.toString();
            }


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingActu = ProgressDialog.show(InsertActualite.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                //super.onPostExecute(s);
                loadingActu.dismiss();
                Log.d("STRING S TOAST",s+"end");
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
                if (s.contains("successfully")) {
                    Intent intent = new Intent(InsertActualite.this,LoginActivity.class);
                    startActivity(intent); }
            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                Log.d("STRING S",s);
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(InsertActualite_URL+s);
                    Document document = Jsoup.connect(url.toString()).get();
                    Log.d("STRING URL",url.toString());
                    String result;

                    result = document.text();
                    Log.d("STRING result",result);
                    return result;
                }catch(Exception e){
                    e.printStackTrace();
                    Log.e("Error thread register", e.getMessage());
                    return null;
                }
            }
        }

        addActualiteclass ru = new addActualiteclass();
        Log.d("url suffix execute",urlSuffix);
        ru.execute(urlSuffix);
    }

}
