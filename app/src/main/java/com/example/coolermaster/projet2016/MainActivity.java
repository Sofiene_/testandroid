package com.example.coolermaster.projet2016;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.coolermaster.projet2016.Actualite.AdapterRecyleViewActualite;
import com.example.coolermaster.projet2016.Actualite.InsertActualite;
import com.example.coolermaster.projet2016.Actualite.JsonActualiteShow;
import com.example.coolermaster.projet2016.Chat.Rooms;
import com.example.coolermaster.projet2016.Draw.draw;
import com.example.coolermaster.projet2016.Draw.listview;
import com.example.coolermaster.projet2016.Evenement.AdapterRecyleViewEvent;
import com.example.coolermaster.projet2016.Login_register.LoginActivity;

import static com.example.coolermaster.projet2016.Evenement.ParserActivity.error;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */


    public static String group = "1" ;
    public static String  userType = "Etudiant" ;
    public static String nameUSER ;



    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static AdapterRecyleViewEvent adapterRecyleViewEvent = new AdapterRecyleViewEvent();
    public static AdapterRecyleViewActualite adapterRecyleViewActualite = new AdapterRecyleViewActualite();

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        TextView t = (TextView) header.findViewById(R.id.user_show_name);
        ImageView v = (ImageView) header.findViewById(R.id.imageView);
        t.setText(nameUSER);
        v.setImageResource(R.mipmap.user_pic_default);


       // context = this;
     //   data = new ArrayList<Evenement>();
   //     classlistView = (ListView) findViewById(R.id.lv);

 //       classlistView.setAdapter(adapter);

        new JsonActualiteShow().execute();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent = new Intent(this,SettingsActivity2.class);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = new Intent (this, LoginActivity.class);

        //Intent intent2 = new Intent (this, Main2Activity.class);
        Intent intent2 = new Intent (this ,Rooms.class);
        if (id == R.id.nav_camera) { //LOGOUT
            userType="Etudiant";
            group="1";
            nameUSER="";
            // Handle the camera action
            startActivity(intent);
        } else if (id == R.id.nav_gallery) { // CHAT
startActivity(intent2);/*
        } else if (id == R.id.nav_slideshow) {
          //   startActivity(intent2);
        } else if (id == R.id.nav_manage) {

*/
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
           args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

View rootView;

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                final RecyclerView rv;
                switch(userType) {
                    case "Enseignant":
                        rootView = inflater.inflate(R.layout.enseignant_fragment_actualite, container, false);
                         rv = (RecyclerView) rootView.findViewById(R.id.list_enseignant_actualite);
                        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rv.setAdapter(adapterRecyleViewActualite);
                        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.addActualite);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(view.getContext(), InsertActualite.class);
                                startActivity(intent);

                            }
                        });
                                break;
                    case "Etudiant":
                        rootView = inflater.inflate(R.layout.etudiant_fragment_actualite, container, false);
                        rv = (RecyclerView) rootView.findViewById(R.id.list_etudiant_actualite);
                        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rv.setAdapter(adapterRecyleViewActualite);

                        break;
                    default : Log.d ("ERROR WRONG USERTYPE",userType);
                        rootView = inflater.inflate(R.layout.etudiant_fragment_actualite, container, false);
                        rv = (RecyclerView) rootView.findViewById(R.id.list_etudiant_actualite);
                        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rv.setAdapter(adapterRecyleViewActualite);

                }

            }
            else
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
                rootView = inflater.inflate(R.layout.fragment_events, container, false);
                final RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.list);
                rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                rv.setAdapter(adapterRecyleViewEvent);

                ///////////////////// DEBUT DIALOG POUR ERREUR DE CONX

                if (error) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());
                    builder.setMessage("Connection to server Error,"+'\n' +"please retry later")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //new ParserActivity().execute();

                                }
                            });
                    builder.show();
                    error = false;
                }

                ///////////////// FIN DIALOG POUR ERREUR DE CONX



            } else
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 3) {
                switch(userType) {
                    case "Enseignant":
                        rootView = inflater.inflate(R.layout.enseignant_fragment_cours, container, false);
                        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.addCours);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                android.support.v7.app.AlertDialog.Builder choose_type_file = new android.support.v7.app.AlertDialog.Builder(getContext());
                                choose_type_file.setTitle("type of the file");
                                choose_type_file.setMessage("Choose the type of file");
                                choose_type_file.setPositiveButton("pdf", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        //      Intent intent = new Intent(getContext(),pdf.class);
                                        //    startActivity(intent);
                                    }});
                                choose_type_file.setNegativeButton("Projete", new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog, int which){
                                        Intent intent = new Intent(getContext(),draw.class);
                                        startActivity(intent);

                                    }
                                });

                                choose_type_file.setNeutralButton("Cancel", new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.cancel();
                                    }
                                });
                                choose_type_file.show();

                            }
                        });

                        ImageButton fich_pdf_img = (ImageButton)rootView.findViewById(R.id.folderpdf);
                        ImageButton fich_projete_img =  (ImageButton)rootView.findViewById(R.id.folderprojete);
                        fich_pdf_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(),pdf.class);
                                startActivity(intent);

                            }
                        });

                        fich_projete_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(),listview.class);
                                startActivity(intent);

                            }
                        });

                        break;
                    case "Etudiant":
                        rootView = inflater.inflate(R.layout.etudiant_fragment_cours, container, false);
                        ImageButton fich_pdf_im = (ImageButton)rootView.findViewById(R.id.folderpd);
                        ImageButton fich_projete_im =  (ImageButton)rootView.findViewById(R.id.folderprojet);
                        fich_pdf_im.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(),pdf.class);
                                startActivity(intent);

                            }
                        });

                        fich_projete_im.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(),listview.class);
                                startActivity(intent);

                            }
                        });

                        break;
                    default : Log.d ("ERROR WRONG USERTYPE",userType);
                        rootView = inflater.inflate(R.layout.etudiant_fragment_cours, container, false);
                }

            } else
           {

               rootView = inflater.inflate(R.layout.fragment_emploi, container, false);


            }


            return rootView;
        }
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Actualité";
                case 1:
                    return "Events";
                case 2:
                    return "Cours";
               // case 3:
                 //   return "Emploi";
            }
            return null;
        }
    }
}
