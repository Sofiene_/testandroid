package com.example.coolermaster.projet2016.Evenement;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.Intent;
import android.net.*;

import com.example.coolermaster.projet2016.R;

import static com.example.coolermaster.projet2016.Evenement.ParserActivity.Evenements;


public class AdapterRecyleViewEvent extends RecyclerView.Adapter<AdapterRecyleViewEvent.MyViewHolder>
{

    @Override
    public int getItemCount() {
        return Evenements.size();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.listcell, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Evenement pair = Evenements.get(position);
        holder.display(pair);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView description;
        private Evenement currentPair = new Evenement();
        public MyViewHolder(final View itemView) {
            super(itemView);
            name = ((TextView) itemView.findViewById(R.id.name));
            description = ((TextView)
                    itemView.findViewById(R.id.description));




            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*class MyWebViewClient extends WebViewClient {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            if (Uri.parse(url).getHost().equals("www.example.com")) {
                                // Designate Urls that you want to load in WebView still.
                                return false;
                            }
                            // Otherwise, give the default behavior (open in browser)
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            itemView.getContext().startActivity(intent);
                            WebView myWebView = (WebView) itemView.findViewById(R.id.webview);
                            myWebView.setWebViewClient(new MyWebViewClient());
                            return true;
                        }
                    }*/


                  /* WebView webView = (WebView) itemView.findViewById(R.id.webview);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.loadUrl("uri");*/
              //      AlertDialog alertDialog = new AlertDialog.Builder(itemView.getContext()).create(); //Read Update
                    AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                    builder.setMessage("Open Browser...")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    String link = "";
                                    link = currentPair.getLink();

                                    Uri uri = Uri.parse(link);
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                   itemView.getContext().startActivity(intent);
                                 //   WebView webView = (WebView) itemView.findViewById(R.id.webview);

//                                    webView.loadUrl("uri");
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    builder.show();
//                    alertDialog.show();   //<-- See This!


                   /* new AlertDialog.Builder(itemView.getContext())
                            .setTitle(currentPair.getDate())
                            .setMessage(currentPair.getText())
                            .show();*/
                }
            });
        }
        public void display(Evenement ev) {
            currentPair = ev;
            name.setText(ev.getDate());
            description.setText(ev.getText());
        }
    }
}