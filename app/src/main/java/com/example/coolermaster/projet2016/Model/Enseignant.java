package com.example.coolermaster.projet2016.Model;

/**
 * Created by COOLER MASTER on 09/12/2016.
 */

public class Enseignant {
    private int id;
    private String name;

    public Enseignant(int id, String name, String last_name) {
        this.id = id;
        this.name = name;
        this.last_name = last_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String last_name;

    @Override
    public String toString() {
        return (name+" "+last_name);
    }
}
