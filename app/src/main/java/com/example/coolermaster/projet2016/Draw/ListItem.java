package com.example.coolermaster.projet2016.Draw;

/**
 * Created by root on 12/16/16.
 */

public class ListItem {

    private String name;
    private String link;

    public ListItem(String name, String link) {
        this.name = name;
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getlink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return this.name + " : " + this.link;
    }

}
